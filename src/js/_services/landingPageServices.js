export const landingPageServices = {
	register,
    imageUpload
}

function handleResponse(response) {
    if (!response.ok) { 
        return Promise.reject(response.statusText);
    }

    return response.json();
}

function register(pressData) {
    let params = {
        'press_heading': pressData['press-title'],
        'meta_title': pressData['meta-title'],
        'meta_description': pressData['meta-desc'],
        'meta_keywords': pressData['meta-keywords'],
        'pressbody': pressData['editorHtml']
    }

    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(params)
    };

    return fetch( window.ctx + '/press/addpress', requestOptions).then(handleResponse);
}

function imageUpload(imageData) {
    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(imageData)
    };

    return fetch( window.ctx + '/press/fileupload', requestOptions).then(handleResponse);
}

