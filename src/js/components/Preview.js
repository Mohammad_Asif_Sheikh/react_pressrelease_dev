import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  showPreview: state.showPreview
});

/*const mapDispatchToProps = (dispatch) => ({
});*/

class Preview extends Component {
  render() {
    const { showPreview } = this.props;
    return (
      <div className="editor-preview">
        <div className="quill ">
          <div className="ql-editor">
            <div dangerouslySetInnerHTML={{ __html: showPreview.showPreviewHtml }} />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(Preview);