import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import store from "./js/_store/index";
import './index.css';
import App from './js/App';
import registerServiceWorker from './registerServiceWorker';
console.log(store);
ReactDOM.render(
	<Provider store={store}>
    	<App />
  	</Provider>, document.getElementById('root'));
registerServiceWorker();