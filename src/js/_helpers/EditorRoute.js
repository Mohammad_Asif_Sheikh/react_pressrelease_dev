import React from 'react';
import { Route, Redirect } from 'react-router-dom';

/*for adding props to the router component*/
const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return (
    React.createElement(component, finalProps)
  );
}

export const EditorRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={routeProps => {
      return renderMergedProps(Component, routeProps, rest);
    }}/>
)