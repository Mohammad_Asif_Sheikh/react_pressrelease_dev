import { combineReducers } from 'redux';

import { showPreview, registration } from './landingPageReducers';
import { alert } from './alertReducers';

const rootReducer = combineReducers({
  showPreview,
  registration,
  alert
});

export default rootReducer;