import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  showPreview: state.showPreview
});

const mapDispatchToProps = (dispatch) => ({
});

class PressForm extends Component {
  constructor (props) {
    super(props);

    this.temp = {
        'press-title': '',
        'press-url': '',
        'meta-title': '',
        'meta-desc': '',
        'meta-keywords': '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange (event) {
    const { name, value } = event.target;

    this.temp = { ...this.temp, [name]: value };
    this.props.pressInfoData(this.temp);
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <form id="press-data-form" name="press-data-form" method="POST">
        <div className="row">
          <div className="input">
            <input type="text" className="form-control" name="press-title" placeholder="Press Title" onChange={this.handleChange} />
          </div>
        </div>
       { /*<div className="row">
                 <div className="input">
                   <input type="text" className="form-control" name="press-url" placeholder="Press URL" onChange={this.handleChange}
                   value={pressData['press-url']} />
                 </div>
               </div>*/}
        <div className="row">
          <div className="input">
            <input type="text" className="form-control" name="meta-title" placeholder="Meta Title" onChange={this.handleChange} />
          </div>
        </div>
        <div className="row">
          <div className="input">
            <input type="text" className="form-control" name="meta-desc" placeholder="Meta Description" onChange={this.handleChange} />
          </div>
        </div>
        <div className="row">
          <div className="input">
            <input type="text" className="form-control" name="meta-keywords" placeholder="Meta Keywords" onChange={this.handleChange} />
          </div>
        </div>
      </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PressForm);